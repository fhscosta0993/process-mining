from pm4py.algo.discovery.alpha import algorithm as alpha_miner
from pm4py.algo.discovery.inductive import algorithm as inductive_miner
from pm4py.algo.discovery.heuristics import algorithm as heuristics_miner

# funcao que aplica o algoritmo de descoberta
def process_discovery(algorithm, log):

    if algorithm == 'alpha':
        net, im, fm = alpha_miner.apply(log)
    
    if algorithm == 'heuristic':
        net, im, fm = heuristics_miner.apply(log, \
            parameters={heuristics_miner.Variants.CLASSIC.value.Parameters.DEPENDENCY_THRESH: 0.99})

    if algorithm == 'inductive':
        net, im, fm = inductive_miner.apply(log)

    return net, im, fm