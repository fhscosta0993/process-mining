from pm4py.visualization.petrinet import visualizer as pn_visualizer

# funcao que salva o modelo em png
def save_model_png(net, im, fm, filepath):
    gviz = pn_visualizer.apply(net, im, fm)
    pn_visualizer.save(gviz, filepath)