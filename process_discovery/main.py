import os
from functions.load_data import load_event_log
from functions.process_discovery import process_discovery
from functions.save_model import save_model_png

filepath = os.getcwd() + '\\process_discovery\\data\\sinteticos.xes'
pngfile = os.getcwd() + '\\process_discovery\\results\\alpha_sinteticos.png'
event_log = load_event_log(filepath)
net, im, fm = process_discovery('alpha', event_log)
save_model_png(net, im, fm, pngfile)
