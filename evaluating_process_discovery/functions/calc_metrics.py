from pm4py.algo.evaluation.replay_fitness import algorithm as replay_fitness_evaluator
from pm4py.algo.evaluation.precision import algorithm as precision_evaluator
from pm4py.algo.evaluation.generalization import algorithm as generalization_evaluator
from pm4py.algo.evaluation.simplicity import algorithm as simplicity_evaluator
from pm4py.algo.analysis.woflan import algorithm as woflan
from pm4py.algo.conformance.alignments import algorithm as alignments

## Função que calcula as métricas de qualidade de um modelo de processo descoberto
def calc_metrics(net, im, fm, log):
    parameters = {replay_fitness_evaluator.token_replay.Parameters.ACTIVITY_KEY: 'concept:name', \
                  replay_fitness_evaluator.alignment_based.Parameters.ACTIVITY_KEY: 'concept:name', \
                  woflan.Parameters.RETURN_ASAP_WHEN_NOT_SOUND: True, woflan.Parameters.PRINT_DIAGNOSTICS: False, \
                  woflan.Parameters.RETURN_DIAGNOSTICS: False, alignments.Parameters.SHOW_PROGRESS_BAR: False}
    is_sound = woflan.apply(net, im, fm, parameters=parameters)
    
    if is_sound == True:
        sound = 'sim'
        fitness = replay_fitness_evaluator.apply(log, net, im, fm, variant=replay_fitness_evaluator.Variants.ALIGNMENT_BASED)
        prec = precision_evaluator.apply(log, net, im, fm, variant=precision_evaluator.Variants.ALIGN_ETCONFORMANCE)
        gen = generalization_evaluator.apply(log, net, im, fm)
        simp = simplicity_evaluator.apply(net)
        f_score = 2 * ((fitness["average_trace_fitness"] * prec) / (fitness["average_trace_fitness"] + prec))
    else:
        sound = 'nao'
        fitness = replay_fitness_evaluator.apply(log, net, im, fm, variant=replay_fitness_evaluator.Variants.TOKEN_BASED)
        prec = precision_evaluator.apply(log, net, im, fm, variant=precision_evaluator.Variants.ETCONFORMANCE_TOKEN)
        gen = generalization_evaluator.apply(log, net, im, fm)
        simp = simplicity_evaluator.apply(net)
        f_score = 2 * ((fitness["average_trace_fitness"] * prec) / (fitness["average_trace_fitness"] + prec))  
        
    return sound, fitness["average_trace_fitness"], prec, gen, simp, f_score