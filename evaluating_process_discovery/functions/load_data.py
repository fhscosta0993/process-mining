import pandas as pd
import pm4py as pm
from pm4py.objects.conversion.log import converter as log_converter

## carregar um log de evento a partir de um dataframe
def load_event_log_df(filepath):
    log_csv = pd.read_csv(filepath)
    parameters = {log_converter.Variants.TO_EVENT_LOG.value.Parameters.CASE_ID_KEY: 'case:concept:name'}
    event_log = log_converter.apply(log_csv, parameters=parameters, \
        variant=log_converter.Variants.TO_EVENT_LOG)
    return event_log

## carregar um log de evento a partir de um arquivo .xes
def load_event_log(filepath):
    event_log = pm.read_xes(filepath)
    return event_log
