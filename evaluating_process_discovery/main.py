import os
import pandas as pd
from functions.load_data import load_event_log_df
from functions.process_discovery import process_discovery
from functions.calc_metrics import calc_metrics

path_data = os.getcwd() + '\\evaluating_process_discovery\\data\\'
path_result = os.getcwd() + '\\evaluating_process_discovery\\results\\'
algorithms = ['alpha', 'heuristic', 'inductive']
type_log = 'sinteticos'
representation = 'frequencias'

for alg in algorithms:
    columns = ['grupo', 'fitness', 'precision', 'generalization', 'simplicity', 'fscore']
    df_results = pd.DataFrame(columns=columns)
    for i in range(2):
        filepath = path_data + representation + "_" + type_log + "_grupo_" + str(i) + "_.csv"
        event_log = load_event_log_df(filepath)
        net, im, fm = process_discovery(alg, event_log)
        sou, fit, pre, gen, sim, fsc = calc_metrics(net, im, fm, event_log)
        row = {'grupo': i, 'fitness': fit, 'precision': pre, \
            'generalization': gen, 'simplicity': sim, 'fscore': fsc}
        df_results = df_results.append(row, ignore_index=True)
    result_file = path_result + alg + "_" + type_log + "_" + representation + "_.csv"
    df_results.to_csv(result_file)

