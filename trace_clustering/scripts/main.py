import pm4py as pm
import pandas as pd
import numpy as np
import os
import functions_df
import functions_clustering
from pm4py.statistics.traces.generic.pandas import case_statistics
from datetime import datetime

# funcao que carrega o evento de log de eventos e retorna:
# o log de evento e o df correspodente
def load_eventlog_df(filename):
    file_path = os.getcwd() + "\\trace_clustering\datas\\" + filename + ".xes"
    events_log = pm.read_xes(file_path)
    df = pm.convert_to_dataframe(events_log)
    return events_log, df

# funcao que carrega um dataframe por meio de um csv
def load_matrix_df(csvfile):
    file_path = os.getcwd() + "\\trace_clustering\datas\\" + csvfile + ".csv"
    df = pd.read_csv(file_path, index_col=0)
    return df

# funcao que cria as representacoes para o algoritmo de agrupamento
# representacoes: binaria e por frequencia absoluta
def create_representations(df):
    binary_df = functions_df.create_binary_representation(df)
    freq_df = functions_df.create_frequency_representation(df)
    return binary_df, freq_df

# salvar uma matrix de representacao
def save_representation(df, name):
    save_path_file = os.getcwd() + "\\trace_clustering\datas\\" + name + ".csv"
    df.to_csv(save_path_file)


# funcao que pega o numero de variantes
def getting_variants(df):
    variants_count = case_statistics.get_variant_statistics(df,
                                          parameters={case_statistics.Parameters.CASE_ID_KEY: "case:concept:name",
                                                      case_statistics.Parameters.ACTIVITY_KEY: "concept:name",
                                                      case_statistics.Parameters.TIMESTAMP_KEY: "time:timestamp"})
    variants_count = sorted(variants_count, key=lambda x: x['case:concept:name'], reverse=True)
    n_variants = len(variants_count)
    return n_variants

# funcao que salva os resultados do experimento
def saving_results(df_results, matrix):
    now = datetime.now()
    now = str(now)
    now_edit = now.replace(" ", "_").replace(".", "_").replace(":", "_").replace("-", "_")
    file_result_name = 'experimento_' + matrix + '_' + now_edit + '_.csv'
    save_result_file = os.getcwd() + "\\trace_clustering\\resultados\\" + file_result_name
    df_results.to_csv(save_result_file)
    
# funcao que retorna a quantidade de dados nos grupos
def n_values_groups(labels):
    (unique, counts) = np.unique(labels, return_counts=True)
    frequencies = np.asarray((unique, counts)).T
    return frequencies

# funcao que salva as frequencias do grupo
def saving_frequencies_groups(frequencies, name):
    df_grp_freq = pd.DataFrame(columns=['grupo', 'freq'], data=frequencies)
    file_group_name = "grupos_" + name + '.csv'
    save_group_file = os.getcwd() + "\\trace_clustering\\resultados\\" + file_group_name
    df_grp_freq.to_csv(save_group_file)

# funcao que ira separar o log de eventos de acordo com o resultado do kmeans
def splitting_matrix(labels, dfX, df, matrix):
    dfX['grupo'] = labels # colocar os rotulos na matrix X
    dfX = dfX.reset_index() # resetar o index da matriz X para termos os id do caso
    new_dfX = dfX[['index', 'grupo']] # pegar somente os id do caso e os rotulos
    new_dfX['index'] = new_dfX['index'].apply(str) # transformando os id do caso em str
    new_dfX = new_dfX.rename(columns={'index': 'case:concept:name'}) # renomeando a coluna de id do caso    
    new_df = pd.merge(df, new_dfX, on="case:concept:name") # realizando o merge com o df do log de evento
    
    save_split_log = os.getcwd() + "\\trace_clustering\\split_events_log\\"

    n = len(new_df['grupo'].unique()) # pegando o total de rotulos diferentes
    for i in range(0, n):
        df_split_event_log = new_df[new_df['grupo'] == i]
        file_split_log = save_split_log + matrix + '_grupo_' + str(i) + '_.csv'
        df_split_event_log.to_csv(file_split_log)
    
    
        


filename = "bpi2017"
csvname = 'matrix_frequencias_reais'
matrix = 'frequencias_reais'

events_log, df = load_eventlog_df(filename)
dfX = load_matrix_df(csvname)
# n_variants = getting_variants(df)
X = dfX.to_numpy()

# df_results = functions_clustering.execute_experiment(X, n_variants)
# saving_results(df_results, matrix)

labels = functions_clustering.execute_best_parameters(X, 4)
freqs = n_values_groups(labels)
name = "binaria_sintetico"
# saving_frequencies_groups(freqs, name)
splitting_matrix(labels, dfX, df, matrix)





