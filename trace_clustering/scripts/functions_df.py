import pandas as pd

## Esta função tem como objetivo inicializar um dataframe zerado para as representações
## o df será formado por colunas, contendo o nome das atividades, e os indexes, 
## contendo o nome dos casos
def create_empty_df(df):
    new_df = df[['case:concept:name', 'concept:name']] # pegando somentes as colunas contendo o id do caso e o nome das atividades
    list_of_activities = sorted(new_df['concept:name'].unique()) # pegando todas as atividades e ordenando
    list_of_cases = sorted(new_df['case:concept:name'].unique()) # pegando todos os ids do caso e ordenando

    # criando um data frame em que as colunas são as atividades e os indexes são os casos
    # no momento, os valores são todos 0
    df_empty = pd.DataFrame(data=0, index=list_of_cases, columns=list_of_activities)
    return df_empty

## Esta função tem como objetivo criar uma representação binária, ou seja,
## caso uma atividade (coluna) aconteça em um determinado caso (linha),
## a célula receberá o valor 1, caso contrário, permanecerá com 0
def create_binary_representation(df):
    cases = df['case:concept:name'].unique() # pegando todos os casos
    binary_df = create_empty_df(df) # copiando o dataframe

    ## Para todo caso:
    ### Pegar as atividades que contém naquele caso no df principal
    ### Assinalar 1 no binary_df de acordo com index e as atividades
    for i in range(len(cases)):
        atividades = list(df[df['case:concept:name'] == cases[i]]['concept:name'])
        binary_df.loc[cases[i]][atividades] = 1

    return binary_df

## Esta função tem como objetivo criar uma representação com base em frequência, 
## ou seja, colocar a frequência absoluta de uma determinada atividade em
## um determinado caso na célula contendo caso e atividade
def create_frequency_representation(df):
    list_of_cases = sorted(df['case:concept:name'].unique()) # lista de casos
    list_of_events = sorted(df['concept:name'].unique()) # lista de atividades
    # matriz de frequencia criada, ainda zerada
    frequency_df = create_empty_df(df)

    for i in list_of_cases: # para cada caso
        for j in list_of_events: # para cada atividade
            # encontrar a frequência absoluta da atividade no caso
            freq = len(df[(df['case:concept:name']==i) & (df['concept:name']==j)])
            frequency_df.loc[i][j] = freq # assinalar a frequencia no dataframe

    return frequency_df