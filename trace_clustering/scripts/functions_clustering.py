from sklearn.cluster import KMeans
from sklearn import metrics
import pandas as pd
import numpy as np
import math

## criar uma função que irá rodar o algoritmo k-means
## com inicialização ++, variando k de 2 até sqrt(n),
## cinco vezes para cada k, sendo avaliado pelo silhouette
def execute_experiment(X, n_variants):
    min_k = 2 # numero minimo de k para os experimentos
    max_k = int(math.sqrt(n_variants)) # numero maximo de k para os experimentos
    # criando um dataframe para salvar os resultados
    df_results = pd.DataFrame(columns=['k', 'sil1', 'sil2', 'sil3', 'sil4', 'sil5', 'avg', 'std'])

    for i in range(min_k, max_k + 1): # número de k
        sil_array = np.empty([1, 5]) # salvar os valores de silhouette no array
        for j in range(5): # número de execuções
            # executando o algoritmo k-means++
            kmeans = KMeans(n_clusters=i, n_init=1).fit(X) 
            labels = kmeans.labels_ # pegando os rótulos
            sil = metrics.silhouette_score(X, labels, metric='euclidean') # medindo o silhouette
            sil_array[0,j] = sil # salvando o valor de silhouette no array
        
        media = np.mean(sil_array) # tirando a media dos silhouettes
        std = np.std(sil_array) # tirando o desvio padrao dos silhouettes
        # criando a linha para salvar no df_results
        row = {'k': i, 'sil1': sil_array[0,0], 'sil2': sil_array[0,1], \
            'sil3': sil_array[0,2], 'sil4': sil_array[0,3], 'sil5': sil_array[0,4], \
                'avg': media, 'std': std}
        # anexando a linha no df_results
        df_results = df_results.append(row, ignore_index = True)
    
    return df_results

## criar função para executar o k-means++ com a melhor parametrização
def execute_best_parameters(X, k):
    kmeans = KMeans(n_clusters=k, n_init=1).fit(X) # executando o algoritmo
    labels = kmeans.labels_ # pegando os rótulos
    return labels


